'''Class defining the screens'''
import pygame #library is used for drawing

class Screens:
    '''this class contains the implementation of all screens'''
    
    def __init__(self):
        self.name = 'NoName'

        #defining the size of the screen
        self.width = 1000
        self.height = 800
        self.screen_size = (1000, 800)
        #defining the background color
        self.backgroundcolor = (255, 255, 255)

    #x -- the x-coordinate of the center of the text block
    #y -- the y-coordinate of the center of the text block
    #size -- the size of the font (in points)
    #color -- color of the text (RGB)
    #screen -- the screen on which to draw
    def text(self, x, y, size, text, color, screen):
        '''this function is used to draw a block of text'''
        font = pygame.font.Font(None, size)
        text = font.render(text, True, color, self.backgroundcolor)
        text_rectangle = text.get_rect()
        text_rectangle.center = (x, y)
        screen.blit(text, text_rectangle)
    #x -- the x-coordinate of the center of the text block
    #y -- the y-coordinate of the center of the text block
    #size -- the size of the font (in p1oints)
    #text1 -- the text to display
    #color -- color of the text (RGB)
    #screen -- the screen on which to draw
    def textoutro(self, x, y, size, text1, color, screen):
        '''function moves text out to the right of the screen'''
        v = 0
        a = 0.01
        i = x
        while i < (x + 1250):
            self.text(i, y, size, text1, self.backgroundcolor, screen)
            i = i + v
            v = v + a
            self.text(i, y, size, text1, color, screen)  
            pygame.display.flip()
    #parnr -- participant number
    #screen -- the screen on which to draw
    def screen_welcome(self, parnr, screen):
        '''function draws the welcome screen'''
        #redrawing the screen
        screen.fill((255,255,255))

        parnr_text = "Your participant number is: " + str(parnr)
        #here the function is actually used, followed by a screen update
        self.text(500, 200, 80, "Welcome", (0, 0, 0,), screen)
        self.text(500, 250, 40, "to the Corsi Block-Tapping Test!", (0, 0, 0), screen)
        self.text(500, 350, 40, parnr_text, (0, 0, 0), screen)
        self.text(500, 580, 40, "Press SPACE to continue", (0, 0, 0), screen)        
        pygame.display.flip() #updates the display

    #screen -- the screen on which to draw
    def screen_description(self, screen):
        '''function draws the screen with description'''
        #redrawing the screen
        screen.fill((255,255,255))
        
        #executing the functions that draw the text
        self.text(500, 200, 40, "In the test, you will see multiple blocks.", (0, 0, 0,), screen)
        self.text(500, 250, 40, "Some of these blocks will light up gray in succession.", (0, 0, 0,), screen)
        self.text(500, 300, 40, "Afterwards, they turn black again.", (0, 0, 0,), screen)
        self.text(500, 350, 40, "When they do, tap the blocks in the same sequence.", (0, 0, 0,), screen)
        self.text(500, 580, 40, "Press SPACE to continue", (0, 0, 0,), screen)
        pygame.display.flip() #updates the display

    #screen -- the screen on which to draw
    def screen_rules(self, screen):
        '''function draws the screen with rules of use'''
        #redrawing the screen
        screen.fill((255,255,255))
        
        #executing the functions that draw the text
        self.text(500, 200, 40, "After each test, you will be shown your results.", (0, 0, 0,), screen)
        self.text(500, 250, 40, "You will see an intermediate screen between each trial.", (0, 0, 0,), screen)
        self.text(500, 420, 40, "Please do not tap blocks that have been tapped already.", (0, 0, 0,), screen)
        self.text(500, 470, 40, "Do not start clicking blocks before the gray blocks have turned black.", (0, 0, 0,), screen)
        self.text(500, 520, 40, "When you repeat the sequence of the same length as shown,", (0, 0, 0,), screen)
        self.text(500, 570, 40, "you will see your result for the trial(s).", (0, 0, 0,), screen)
        self.text(500, 650, 40, "Press SPACE to start the task.", (0, 0, 0,), screen)
        pygame.display.flip() #updates the display

    #testnr -- the number of the tests that are already finished
    #score -- the score the participant currently has
    #screen -- the screen on which to draw
    #time -- time spent on the task
    def screen_intermediate(self, testnr, score, screen, time):
        '''function draws the screen with intermediate results'''
        #redrawing the screen
        screen.fill((255,255,255))
        
        #variables for converting parameters into text
        testnr_text = "Trial number: " + str(testnr)
        score_text = "Current score: " + str(score)
        time_text = "Time spent on the task: " + str(time)

        #executing the functions that draw the text
        self.text(500, 200, 80, "Intermediate results", (0, 0, 0,), screen)
        self.text(500, 300, 40, testnr_text, (0, 0, 0,), screen)
        self.text(500, 350, 40, score_text, (0, 0, 0,), screen)
        self.text(500, 400, 40, time_text, (0, 0, 0,), screen)
        self.text(500, 580, 40, "Press SPACE to start the next trial.", (0, 0, 0,), screen)
        pygame.display.flip() #updates the display

    #trialnr -- number of trials completed
    #finalscore -- final score of the participant
    #screen -- the screen on which to draw
    def screen_final(self, trialnr, finalscore, screen):
        '''draws the final screen, summarizing all the results'''
        #redrawing the screen
        screen.fill((255,255,255))
        
        #variables for converting parameters into text
        trialnr_text = "Number of trials completed: " + str(trialnr)
        finalscore_text = "Corsi span: " + str(finalscore)

        #executing the functions that draw the text
        self.text(500, 180, 80, "Final score", (0, 0, 0,), screen)
        self.text(500, 250, 40, trialnr_text, (0, 0, 0,), screen)
        self.text(500, 300, 40, finalscore_text, (0, 0, 0,), screen)
        self.text(500, 580, 40, "Press SPACE to continue.", (0, 0, 0,), screen)
        pygame.display.flip() #updates the display

    #screen -- the screen on which to draw
    def screen_goodbye(self, screen):
        '''draws the final screen, summarizing all the results'''
        #redrawing the screen
        screen.fill((255,255,255))
        
        #executing the functions that draw the text
        self.text(500, 200, 80, "Thank you for participating!", (0, 0, 0,), screen)
        self.text(500, 350, 40, "Press SPACE to send your results to the researcher.", (0, 0, 0,), screen)
        self.text(500, 400, 40, "Have a nice day!", (0, 0, 0,), screen)

        pygame.display.flip() #updates the display