'''file for enabling saving to file option'''
#fil -- file to which we write the results
#length -- the length of sequence that user repeated without any mistakes
#seqtime -- time spent by user on the sequence
#num_of_err -- number of errors made by the user
#trial_num -- number of the trial
#score -- score of the user on the sequence
def filelog(fil, length, seqtime, num_of_err, score, trial_num):
    '''saves user data into a file'''
    fil.write("Trial number: " + str(trial_num) + "\n")
    fil.write("Correctly repeated from the beginning sequence is " + str(length) + " blocks" + "\n")
    fil.write("Time spent on this sequence is " + str(round(seqtime, 2)) + " sec" + "\n")
    fil.write("Number of mistakes made is " + str(round(num_of_err, 2)) + "\n")
    fil.write("Score is " + str(round(score, 2)) + "\n")
    fil.write("\n")

def main ():
    ''''main function for testing purposes'''
    '''max_length = 5
    seq1time = 2.45
    seq2time = 5.32
    seq3time = 3.42
    seq4time = 35
    finalscore = 69
    user_id = 5
    filelog(user_id, max_length, seq1time, seq2time, seq3time, seq4time, finalscore)'''

#actual testing
if __name__ == '__main__':
    main()
