# Programming Project Group 14
# Corsi Block Tapping Task
## Introduction
This project contains an implementation of the Programming Project of Group 14 of module Human Factors and Engineering Psychology. 
<p>Contributors to this project are:</p>
<p>1. Alina Maximova (s2032074, a.maximova@student.utwente.nl)</p>
<p>2. Jana Westermann (s2085887, j.m.l.westermann@student.utwente.nl)</p>
<p>3. Sylvan Botter (s2299100, s.botter@student.utwente.nl)</p>
<p> </p>
<p>This implementation contains several files:</p>
<p>1. Block.py -- contains the class defining a block and its properties and functions</p>
<p>2. game.py -- contains the executable program for running the Task (first version, not the Corsi)with a user</p>
<p>3. save.py -- contains the method which saves the user's results to a separate file</p>
<p>4. sequence_update.py -- contains the class defining four sequences and their properties and functions</p>
<p>5. user_sequence.py -- contains the method which detects what block was tapped by the user</p>
<p>6. README.md -- contains the description of the whole repository</p>
<p>7. sequence.csv -- contains the sequence predefined in a separate file</p>
<p>8. screens_class -- contains the class defining all the functions regarding screens</p>
<p>9. corsi.py -- contains the Corsi program with showing only the sequences</p>
<p>10. Corsi_Block_Tapping_Task.py -- contains the Corsi task implementation as the project description suggests</p>
<p> </p>
<p>Also, this archive contains the following files which are not necesssary for running the program, but are important for the project:</p>
<p>1. Transitional_table.pdf -- contains the transitional table of the implementation</p>
<p>2. screens.py -- contains the first version of screen functions, tested in this file</p>

## Dependencies
In order to run this project a user must have the following libraries installed:
<p>1. pygame</p>
<p>2. time</p>
<p>3. sys</p>
<p>4. random</p>
<p>5. numpy</p>
The version of Python used: Python 3.8.5 64-bit (conda)
<p> NOTE: this project was created with the help of Anaconda</p>

## How to run the program
<p>1. Extract the folder "programming_project_group_14_master"</p>
<p>2. Open the extracted folder from VSCode or an alternative program</p>
<p>3. Install the dependencies</p>
<p>4. Open and run Corsi_Block_Tapping_Task.py file</p>
<p>5. Follow the instructions on the screen</p>

## What happens when you run the program
This program is made to, first of all, show the Corsi Block Tapping Task and, secondly, to provide researchers with all possible extra tools which can help them in their projects. This project has a great flexibility and thus, can be beneficial for a wide range of people.

## Additional information
This project was created using GitLab. Please, use the following link to see the repository: https://gitlab.utwente.nl/s2032074/programming_project_group_14/-/tree/master 
