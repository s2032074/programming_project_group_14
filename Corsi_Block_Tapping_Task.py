'''this file contains the Corsi task implementation'''

#pygame -- library for drawing
#time -- library that works with time
import time
import pygame

#importing the predefined classes
from sequence_update import Sequence
from user_sequence import detect_block_ID
from save import filelog
from screen_class import Screens

def main():
    '''This is the body of the main executable program'''
    #flag - variable for working with screens
    flag = True

    #insert the number of the participant
    user_number = input("Insert your number here: ")

    #keeps track of the number of blocks in a sequence
    number_of_blocks = 2

    #defining the screen parameters
    pygame.init()
    width = 1000
    height = 800
    pygame.display.set_mode((width, height))
    screen = pygame.display.get_surface()
    screen.fill((255,255,255))

    #adding screens
    screens = Screens()
    screens.screen_welcome(user_number, screen)

    #transition to the next screen
    while flag:
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
                screens.screen_description(screen)
                flag = False

    flag = True

    #creating a file fot the user
    filename = "results_user_" + str(user_number) + ".txt"
    f = open(filename, "x")
    f.write("user ID is " + str(user_number) + "\n")

    #transition to the next screen
    while flag:
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
                screens.screen_rules(screen)
                flag = False

    flag = True

    #transition to the next screen
    while flag:
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
                flag = False
    flag = True
    #redrawing the screen
    screen.fill((255,255,255))

    #number of errors made by user
    err_num = 0
    trial_num = 0 #number of trials
    score = 0 #score of the user

    while err_num < 2:
        if number_of_blocks > 9:
            number_of_blocks = 9
        trial_num = trial_num + 1
        sequence = Sequence(0)
        sequence.create_sequence()
        sequence.draw_sequence_comp(screen, number_of_blocks)

        #creating a list for tracking user input
        user_seq = []

        all_blocks_tapped = 0

        #start a timer
        time_start = time.time()
        #interaction with the user
        while all_blocks_tapped < number_of_blocks:
            for event in pygame.event.get():
                sequence.draw_sequence_human(screen, event)
                k = detect_block_ID(sequence.sequence, event)
                if k != None:
                    user_seq.append(sequence.dict[detect_block_ID(sequence.sequence, event)])
                    all_blocks_tapped = all_blocks_tapped + 1
                pygame.display.update()

        #end of interaction with the user, stop timer and calculate total time
        time_end = time.time()
        #total time spent on this sequence
        seqtime = time_end - time_start
        err = sequence.compare_sequences(user_seq)
        score = len(user_seq)
        
        #checking the number of errors
        if err > 0:
            err_num = err_num + 1
            score = score - 1
        if err == 0:
            number_of_blocks = number_of_blocks + 1
            err_num = 0
        #saving results to the file
        filelog(f, score, seqtime, err, score, trial_num)
        #if err_num < 3 then show intermediate screen
        if err_num < 2:

            #transition to the next screen
            screens.screen_intermediate(trial_num, score, screen, round(seqtime, 2))

            #transition to the next screen
            while flag:
                for event in pygame.event.get():
                    if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
                        flag = False

            flag = True

        #redrawing the screen
        screen.fill((255,255,255))
    
   
    #transition to the next screen
    screens.screen_final(trial_num, score, screen)
    #transition to the next screen
    while flag:
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
                flag = False

    flag = True

    #transition to the next screen
    screens.screen_goodbye(screen)
    while flag:
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
                flag = False

    flag = True
    #saving results to file
    f.close()
    pygame.quit()


#start corsi tapping task
if __name__ == '__main__':
    main()
