
import pygame
pygame.init()
STATE = "welcome"

    
def main():
    #setting things up
    width = 1000
    height = 800
    backgroundcolor = ((255, 255, 255))
    pygame.display.set_mode((width, height))
    screen = pygame.display.get_surface()
    screen.fill(backgroundcolor)
    pygame.display.set_caption("Corsi block tapping test")
    if STATE == "welcome":
        screen_welcome()
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
def screen_welcome(parnr):
    parnr_text = "your participant number is:" + str(parnr)

    #these functions are used for drawing text
    def text(x, y, size, text, color,):
        font = pygame.font.Font(None, size)
        text = font.render(text, True, color, backgroundcolor)
        text_rectangle = text.get_rect()
        text_rectangle.center = (x, y)
        screen.blit(text, text_rectangle)
    #here the function is actually used, followed by a screen update
    text(500, 200, 80, "Welcome", (0, 0, 0,))
    text(500, 250, 40, "to the Corsi Block-Tapping Test!", (0, 0, 0))
    text(500, 350, 40, parnr_text, (0, 0, 0))
    text(500, 580, 40, "Press SPACE to continue", (0, 0, 0))               
    pygame.display.update()
def screen_desc():
    #again, drawing text. it's more useful to have this function outside the whole thing of course, but this works for now
    def text(x, y, size, text, color,):
        font = pygame.font.Font(None, size)
        text = font.render(text, True, color, backgroundcolor)
        text_rectangle = text.get_rect()
        text_rectangle.center = (x, y)
        screen.blit(text, text_rectangle)
    #executing the functions that draw the text
    text(500, 200, 40, "In the test, you will see multiple blocks.", (0, 0, 0,))
    text(500, 250, 40, "These blocks will light up gray in succession.", (0, 0, 0,))
    text(500, 300, 40, "Afterwards, they turn black again.", (0, 0, 0,))
    text(500, 350, 40, "When they do, tap the blocks in the same sequence.", (0, 0, 0,))
    text(500, 580, 40, "Press SPACE to continue", (0, 0, 0,))
    pygame.display.update()

def screen_rules():
    #F U N C T I O N S
    def text(x, y, size, text, color,):
        font = pygame.font.Font(None, size)
        text = font.render(text, True, color, backgroundcolor)
        text_rectangle = text.get_rect()
        text_rectangle.center = (x, y)
        screen.blit(text, text_rectangle)
    #drawing dat text
    text(500, 200, 40, "There will be 4 trials.", (0, 0, 0,))
    text(500, 250, 40, "After each test, you will be shown your results.", (0, 0, 0,))
    text(500, 300, 40, "You will see an intermediate screen between each trial.", (0, 0, 0,))
    text(500, 350, 40, "After all 4 tests, you will be shown your total results.", (0, 0, 0,))
    text(500, 420, 40, "Please do not tap blocks that have been tapped already.", (0, 0, 0,))
    text(500, 470, 40, "Do not start clicking blocks before all blocks have turned black.", (0, 0, 0,))
    text(500, 580, 40, "Press SPACE to start the task.", (0, 0, 0,))
def screen_intermediate(testnr, time, correctseq, errornr):
    #variables for converting parameters into text
    testnr_text = "this was test: " + str(testnr)
    time_text = "time spent: " + str(time)
    correctseq_text = "Number of correct blocks from the beginning: " + str(correctseq)
    errornr_text = "Number of errors made: " + str(errornr)
    #F U N C T I O N S
    def text(x, y, size, text, color,):
        font = pygame.font.Font(None, size)
        text = font.render(text, True, color, backgroundcolor)
        text_rectangle = text.get_rect()
        text_rectangle.center = (x, y)
        screen.blit(text, text_rectangle)
    #drawing dat text
    text(500, 200, 80, "Intermediate results", (0, 0, 0,))
    text(500, 300, 40, testnr_text, (0, 0, 0,))
    text(500, 350, 40, time_text, (0, 0, 0,))
    text(500, 400, 40, correctseq_text, (0, 0, 0,))
    text(500, 450, 40, errornr_text, (0, 0, 0,))
    text(500, 580, 40, "Press SPACE to start the next trial.", (0, 0, 0,))
    pygame.display.update()
def screen_final(time1, time2, time3, time4, correctseqmax, finalscore):
    #variables for converting parameters into text
    time_text1 = "your time on trial 1: " + str(time1) + " seconds"
    time_text2 = "your time on trial 2: " + str(time2) + " seconds"
    time_text3 = "your time on trial 3: " + str(time3) + " seconds"
    time_text4 = "your time on trial 4: " + str(time4) + " seconds"
    correctseq_text = "maximum of correct blocks from the start: " + str(correctseqmax)
    finalscore_text = "your final score is: " + str(finalscore)
    #F U N C T I O N S
    def text(x, y, size, text, color,):
        font = pygame.font.Font(None, size)
        text = font.render(text, True, color, backgroundcolor)
        text_rectangle = text.get_rect()
        text_rectangle.center = (x, y)
        screen.blit(text, text_rectangle)
    #drawing dat text
    text(500, 180, 80, "Final results", (0, 0, 0,))
    text(500, 250, 40, time_text1, (0, 0, 0,))
    text(500, 300, 40, time_text2, (0, 0, 0,))
    text(500, 350, 40, time_text3, (0, 0, 0,))
    text(500, 400, 40, time_text4, (0, 0, 0,))
    text(500, 450, 40, correctseq_text, (0, 0, 0,))
    text(500, 500, 40, finalscore_text, (0, 0, 0,))
    text(500, 580, 40, "Press SPACE to exit.", (0, 0, 0,))
    pygame.display.update()
def screen_goodbye():
    #F U N C T I O N S
    def text(x, y, size, text, color,):
        font = pygame.font.Font(None, size)
        text = font.render(text, True, color, backgroundcolor)
        text_rectangle = text.get_rect()
        text_rectangle.center = (x, y)
        screen.blit(text, text_rectangle)
    #drawing dat text
    text(500, 200, 80, "Thank you for participating!", (0, 0, 0,))
    text(500, 350, 40, "The application will close automatically.", (0, 0, 0,))
    text(500, 400, 40, "Have a nice day!", (0, 0, 0,))
main()
